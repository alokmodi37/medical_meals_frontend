const express = require('express'),
	app = express(),
	path = require('path'),
	logger = require('morgan');

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static('./'));

app.listen(5000, function(){
	console.log('Server is listening on the port...', 5000);
});
